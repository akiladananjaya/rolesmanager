import React from 'react';
import PrimaryTitle from '../shared/PrimaryTitle';
import PrimaryTextInput from '../shared/PrimaryTextInput/index';
import PrimaryButton from '../shared/PrimayButton/index';
import './user-edit.css';
import DemoDataContext from '../../contexts/DemoDataContext';

const UserEdit = ({ _email, _roles, _username, _modalClose }) => {

    const data = React.useContext(DemoDataContext);

    const [email, setEmail] = React.useState(_email);
    const [roles, setRoles] = React.useState(_roles.join(','))

    const editUser = () => {
        var tmpU = Object.assign({}, data.users);
        tmpU[_username] = {
            username: _username,
            email: email,
            roles: roles.split(",")
        }
        data.setUsers(tmpU)
        _modalClose();
    }



    return (
        <div className="userEdit">
            <PrimaryTitle title="Edit user" />
            <PrimaryTextInput placeholder={email} onChangeX={(e) => { setEmail(e.target.value) }} />
            <PrimaryTextInput placeholder={roles} onChangeX={(e) => { setRoles(e.target.value) }} />
            <PrimaryButton value="Save" onPress={() => { editUser() }} />

        </div>
    )
}

export default UserEdit;