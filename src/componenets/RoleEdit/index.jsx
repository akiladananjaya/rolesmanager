import React from 'react';
import PrimaryTitle from '../shared/PrimaryTitle';
import PrimaryTextInput from '../shared/PrimaryTextInput/index';
import PrimaryButton from '../shared/PrimayButton/index';
import './role-edit.css';
import DemoDataContext from '../../contexts/DemoDataContext';

const UserEdit = ({ _rolename, _permissions, _modalClose }) => {

    const [rolename, setRolename] = React.useState(_rolename);
    const [permissions, setPermissions] = React.useState(_permissions.join(","));

    const data = React.useContext(DemoDataContext)

    const editRole = () => {

        if (permissions.length == 0) {
            return;
        }

        var tmpR = Object.assign({}, data.roles);
        tmpR[_rolename] = {
            rolename: _rolename,
            permissions: permissions.split(",")
        }
        data.setRoles(tmpR);
        _modalClose();
    }


    return (
        <div className="userEdit">
            <PrimaryTitle title="Edit role" />
            {/* <PrimaryTextInput placeholder={rolename} onChangeX={(e) => { setRolename(e.target.value) }} /> */}
            <PrimaryTextInput placeholder={permissions} onChangeX={(e) => { setPermissions(e.target.value) }} />
            <PrimaryButton value="Save" onPress={() => { editRole() }} />

        </div>
    )
}

export default UserEdit;