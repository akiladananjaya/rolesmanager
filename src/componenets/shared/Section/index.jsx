import React from 'react';
import PrimaryTitle from '../PrimaryTitle/index';
import './section.css';

const Section = ({ children, title }) => {
    return (
        <>
            <div className="sectionComp">
                <PrimaryTitle title={title} />
                {children}
            </div>
        </>

    );
}

export default Section;