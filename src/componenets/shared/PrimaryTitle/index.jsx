import React from 'react';
import './primary-title.css';

const PrimaryTitle = ({ title }) => {
    return (
        <h3 className="title">{title}</h3>
    );
}

export default PrimaryTitle;