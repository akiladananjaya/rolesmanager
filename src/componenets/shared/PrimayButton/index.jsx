
import React from 'react';
import './primary-button.css';

const PrimaryButton = ({ value, onPress }) => {
    return (
        <input className="input-button" type="button" value={value} onClick={onPress} />
    )
}

export default PrimaryButton;