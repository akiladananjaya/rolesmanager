
import React from 'react';
import './primary-text-input.css';

const PrimaryTextInput = ({ placeholder, value, onChangeX }) => {
    return (
        <input className="input-text" type="text" value={value} placeholder={placeholder} onChange={onChangeX} />
    )
}

export default PrimaryTextInput;