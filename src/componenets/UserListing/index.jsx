import React from 'react';
import './user-listing.css';
import PrimaryTitle from '../shared/PrimaryTitle/index';
import { MdDelete, MdModeEdit } from 'react-icons/md';
import Section from '../shared/Section/index';
import PrimaryTextInput from '../shared/PrimaryTextInput/index';
import PrimaryButton from '../shared/PrimayButton/index';
import Popup from "reactjs-popup";
import UserEdit from '../../componenets/UserEdit/index';
import DemoDataContext from '../../contexts/DemoDataContext';

const UserListing = () => {
    const data = React.useContext(DemoDataContext);

    const deleteUser = (username) => {
        var tmpU = Object.assign({}, data.users);
        delete tmpU[username];
        data.setUsers(tmpU);

    }

    return (
        <>
            <Section title="Current users">
                <table>
                    <thead>
                    </thead>
                    <tbody>
                        {

                            Object.values(data.users).map((user) => {
                                return (
                                    <tr className="userTR" key={user.username}>
                                        <td className="titleTD">
                                            <div>
                                                {user.username} - {user.email}
                                                <br />
                                                <span className="permissionComp">
                                                    Roles: {user.roles.join(',')}</span>
                                            </div>
                                        </td>
                                        <td className="actionTD actionDelete" onClick={() => { deleteUser(user.username) }}><MdDelete /></td>
                                        <Popup modal trigger={<td className="actionTD actionEdit"><MdModeEdit /></td>} position="right center">
                                            {(close) => (
                                                <UserEdit _username={user.username} _email={user.email} _roles={user.roles} _modalClose={close} />
                                            )
                                            }
                                        </Popup>

                                    </tr>
                                )
                            }
                            )
                        }
                    </tbody>
                    <tfoot></tfoot>
                </table>
            </Section>

        </>
    )
}

export default UserListing;