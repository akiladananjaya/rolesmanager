import React from 'react';
import './role-listing.css';
import { MdDelete, MdModeEdit } from 'react-icons/md';
import Popup from "reactjs-popup";
import RoleEdit from '../../componenets/RoleEdit/index';
import DemoDataContext from '../../contexts/DemoDataContext';


const RoleListing = () => {

    const data = React.useContext(DemoDataContext)

    const deleteRole = (rolename) => {
        var tmpR = Object.assign({}, data.roles);
        delete tmpR[rolename];
        data.setRoles(tmpR);
    }


    return (
        <table>
            <thead>
            </thead>
            <tbody>
                {

                    Object.values(data.roles).map((role) => {
                        return (
                            <tr className="userTR" key={role.rolename}>
                                <td className="titleTD">
                                    <div>
                                        {role.rolename}
                                        <br />
                                        <span className="permissionComp">
                                            Permissions: {role.permissions.join(',')}</span>
                                    </div>
                                </td>
                                <td className="actionTD actionDelete" onClick={() => { deleteRole(role.rolename) }}><MdDelete /></td>
                                <Popup modal trigger={<td className="actionTD actionEdit"><MdModeEdit /></td>} position="right center">
                                    {(close) => (
                                        <RoleEdit _rolename={role.rolename} _permissions={role.permissions} _modalClose={close} />
                                    )
                                    }
                                </Popup>

                            </tr>
                        )
                    }
                    )
                }
            </tbody>
            <tfoot></tfoot>

        </table>
    )
}

export default RoleListing;