import React from 'react';
import './header.css';

const Header = () => {
    return (
        <div className="header">
            <h3 className="headerTitle">Roles Managment System</h3>
        </div>
    )
}

export default Header;