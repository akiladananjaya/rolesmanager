var allUsers = {
    user1: {
        username: "user1",
        email: "user@domain.com",
        roles: ["admins", "roots"]
    },
    user2: {
        username: "user2",
        email: "user2@domain.com",
        roles: ["defaultuser", "roots"]
    }


}

var allRoles = {
    admin: {
        rolename: "admin",
        permissions: ["create", "write"]
    },
}


module.exports = {
    allUsers,
    allRoles
}