import React from 'react';
import './role-page.css';
import UserListing from '../../componenets/UserListing/index';
import Section from '../../componenets/shared/Section/index';
import PrimaryTextInput from '../../componenets/shared/PrimaryTextInput';
import PrimaryButton from '../../componenets/shared/PrimayButton/index';
import RoleListing from '../../componenets/RoleListing/index';

import DemoDataContext from '../../contexts/DemoDataContext';


const RolePage = () => {

    const data = React.useContext(DemoDataContext)

    const [rolename, setRolename] = React.useState("");
    const [permissions, setPermissions] = React.useState("");

    const addRole = () => {
        var tmpR = Object.assign({}, data.roles);
        tmpR[rolename] = {
            rolename: rolename,
            permissions: permissions.split(","),
        }
        data.setRoles(tmpR)
        console.log(data.roles)
    }

    return (
        <div className="userPage">
            <Section title="Add a new Role">
                <form>
                    <PrimaryTextInput placeholder="Role name ex: admin" onChangeX={(e) => { setRolename(e.target.value) }} />
                    <PrimaryTextInput placeholder="Permissions ex: create,update" onChangeX={(e) => { setPermissions(e.target.value) }} />
                    <PrimaryButton value="ADD" onPress={() => addRole()} />
                </form>
            </Section>
            <Section title="Current roles">
                <RoleListing />
            </Section>
        </div>
    );
}

export default RolePage;
