import React, { useState } from 'react';
import './user-page.css';
import UserListing from '../../componenets/UserListing/index';
import Section from '../../componenets/shared/Section/index';
import PrimaryTextInput from '../../componenets/shared/PrimaryTextInput';
import PrimaryButton from '../../componenets/shared/PrimayButton/index';
import { users, allUsers } from '../../helpers/simulate';
import DemoDataContext from '../../contexts/DemoDataContext';


const UserPage = () => {

    const [username, setUsername] = React.useState("");
    const [email, setEmail] = React.useState("");

    const data = React.useContext(DemoDataContext);

    const addUser = () => {
        var tmpU = Object.assign({}, data.users);
        tmpU[username] = {
            username: username,
            email: email,
            roles: ["defaultuser"],
        }
        data.setUsers(tmpU)
    }

    return (
        <div className="userPage">
            <Section title="Add a new user">
                <form>
                    <PrimaryTextInput placeholder="Username" onChangeX={(e) => { setUsername(e.target.value) }} />
                    <PrimaryTextInput placeholder="Email Address" onChangeX={(e) => { setEmail(e.target.value) }} />
                    <PrimaryButton value="ADD" onPress={() => addUser()} />
                </form>
            </Section>
            <UserListing />
        </div>
    );
}

export default UserPage;
