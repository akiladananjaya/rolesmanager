import React from 'react'
import { NavLink } from 'react-router-dom';
import './navbar.css';
import { useLocation } from 'react-router-dom'


const Navbar = () => {

    let location = useLocation();

    return (
        <div>
            <div className="sideMenu">
                {/* <span className={location.pathname === "/" ? 'activeMenuItem menuLink' : "menuLink"}>
                    <NavLink activeClassName="active" to="/">Home </NavLink>
                </span> */}
                <span className={location.pathname === "/users" ? 'activeMenuItem menuLink' : "menuLink"}>
                    <NavLink activeClassName="active" to="/users">Users</NavLink>
                </span>
                <span className={location.pathname === "/roles" ? 'activeMenuItem menuLink' : "menuLink"}>
                    <NavLink activeClassName="active" to="/roles">Roles </NavLink>
                </span>
            </div>
        </div>
    );
};

export default Navbar;