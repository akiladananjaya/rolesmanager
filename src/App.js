import React from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import UserPage from './Pages/UserPage/index';
import RolePage from './Pages/RolePage/index';
import NavBar from './NavBar/index';
import Header from './componenets/Header/index';
import { allRoles, allUsers } from './helpers/simulate';
import DemoDataContext from './contexts/DemoDataContext';
import { Redirect } from "react-router-dom";
import HomePage from './Pages/HomePage/index';


function App() {

  const [users, setUsers] = React.useState(allUsers);
  const [roles, setRoles] = React.useState(allRoles);

  const redirectToUsers = () => {
    return (
      <Redirect to="/users" />
    )
  }


  return (
    <>
      <Header />
      <DemoDataContext.Provider value={{
        users, setUsers,
        roles, setRoles,
      }}>
        <div className="contentWrapper">
          <NavBar />
          <Switch>
            <Route path="/" component={redirectToUsers} exact />
            <Route path="/users" component={UserPage} />
            <Route path="/roles" component={RolePage} />
          </Switch>
        </div>
      </DemoDataContext.Provider>

    </>
  );
}

export default App;
